(function() {
    var main = {
        menuToggle: function() {
            $('.menuToggle').on('click', function() {
                if ($(this).hasClass('is-active')) {
                    $(this).removeClass('is-active');
                    $('nav').removeClass('is-open');
                    $("header").removeClass("is-o");
                } else {
                    console.log('click');
                    $(this).addClass('is-active');
                    $('nav').addClass('is-open');
                    $("header").addClass("is-o");
                }
            });
            if (window.innerWidth > 768) {
                $("header nav li.news").hover(function() {
                    $(this).find(".sub").stop().slideToggle();
                });
            } else {
                $("header nav li.news").click(function() {
                    $(this).find(".sub").stop().slideToggle();
                });
            }
        },
        index_slide: function() {
            $(".banners").bxSlider({
                mode: 'fade',
                auto: true,
                autoControls: false,
                pause: 4000,
                nextText: '<img src="assets/images/silde_arr_right.png">',
                prevText: '<img src="assets/images/silde_arr_left.png">',
            });
        },
        index_more: function() {
            var fac = $(".facility");
            var more_list = fac.find(".more_list");
            var imgs = fac.find(".items");
            var dot = fac.find(".c_dot");
            imgs.find(".image").eq(0).addClass("active");
            more_list.find("li").eq(0).addClass("active");
            dot.find(".dot").eq(0).addClass("active");
            more_list.find("li").each(function(inx) {
                $(this).click(function() {
                    if (inx != more_list.find("li").index(more_list.find("li.active"))) {
                        more_list.find("li").removeClass("active");
                        imgs.find(".image").removeClass("active");
                        dot.find(".dot").removeClass("active");
                        more_list.find("li").eq(inx).addClass('active');
                        imgs.find(".image").eq(inx).addClass('active');
                        dot.find(".dot").eq(inx).addClass('active');
                    }
                });
            });
        },
        init: function() {
            // var m_left = $("nav ul.left");
            // var m_right = $("nav ul.right");
            // m_left.css("width", "auto");
            // m_right.css("width", "auto");
            // if (m_left.width() > m_right.width()) {
            //     m_right.width(m_left.width());
            // } else {
            //     m_left.width(m_right.width());
            // }
        },
        event_init: function() {
            var og = $("nav .logo").offset().top;
            
            $(window).scroll(function() {
                if($(window).innerWidth() > 1023){
                    if ($(window).scrollTop() > og) {
                        $("header").addClass("fixed");
                        $(".main-wrapper").addClass('fixHeader');
                    } else {
                        $("header").removeClass("fixed");
                        $(".main-wrapper").removeClass('fixHeader');
                    }
                    main.fix_scroll();
                    main.fix_scroll2();
                }
            });
            $("nav .lan_btn").click(function() {
                $(this).find("ol").slideToggle();
                $(this).toggleClass("open");
            });
        },
        news_in: function() {
            var news = $(".news_in");
            if (window.innerWidth < 768) {
                news.find(".other .news_list").bxSlider({
                    mode: 'horizontal',
                    auto: true,
                    pause: 4000,
                    controls: false
                });
            }
            $(".in02 .left_img .slide_div").bxSlider({
                mode: 'horizontal',
                auto: true,
                pause: 4000,
                prevSelector: '.cont_btn .pre',
                nextSelector: '.cont_btn .next',
                prevText: '<img alt="" src="assets/images/silde_arr_left2.png">',
                nextText: '<img alt="" src="assets/images/silde_arr_right2.png">',
                onSliderLoad: function(){
                    main.detect_verticleAlign();
                }
            });
            $(".spa .left_img .slide_div").bxSlider({
                mode: 'horizontal',
                auto: true,
                pause: 4000,
                prevText: '<img alt="" src="assets/images/silde_arr_left.png">',
                nextText: '<img alt="" src="assets/images/silde_arr_right.png">'
            });
        },
        food_load: function() {
            var pop = $("#pop_view");
            var scroll_h = 0;
            $(".load_food").click(function(event) {
                event.preventDefault();
                pop.fadeIn(300);
                var url = $(this).attr("href");
                pop.find("#content").load(url, function() {
                    $(this).find(".close").click(function() {
                        pop.fadeOut(300);
                        pop.find("#content").empty();
                    });
                    $(this).find(".left_img img").imagesLoaded().done(function() {
                        pop.find(".left_img").bxSlider({
                            auto: true,
                            pause: 4000,
                            mode: 'horizontal'
                        });
                        check_size();
                    });
                    pop.fadeIn(300);
                });
            });

            function check_size() {
                if (window.innerWidth > 768) {
                    scroll_h = pop.find(".left_img img").height();
                    pop.find(".right_div").mCustomScrollbar({
                        setHeight: scroll_h
                    });
                } else {}
            }
        },

        fix_scroll: function() {
            var max_height = 0;
            var max_inx = 0;
            var set_top = 0;
            var main_c = $("#scroll_cont");
            main_c.find(".sc").each(function(inx) {
                if ($(this).height() > max_height) {
                    max_height = $(this).height();
                    max_inx = inx;
                    set_top = $(this).offset().top;
                }
            });
            main_c.find(".sc").each(function(ix) {
                if (ix != max_inx && $(this).height() + $(window).scrollTop() < max_height) {
                    // console.log(max_height);
                    $(this).css("margin-top", $(window).scrollTop());
                }
            });
            if ($(window).scrollTop() == 0) {
                main_c.find(".sc").css("margin-top", "auto");
            }
        },
        fix_scroll2: function() {
            var max_height = 0;
            var max_inx = 0;
            var set_top = 0;
            var news_c = $(".news_in");
            var news_c_img = news_c.find('.left_img');
            var news_c_info = news_c.find('.right_info');
            if(news_c_info.outerHeight() > news_c_img.outerHeight()){
                max_height = news_c_info.outerHeight();
                set_top = news_c_info.offset().top;
                max_inx = news_c_img.outerHeight() + $(window).scrollTop();
            }
            if( max_inx < max_height){
                news_c_img.css("margin-top", $(window).scrollTop());
            }
        },
        detect_verticleAlign: function() {
            var news_c = $(".news_in");
            var news_c_img = news_c.find('.left_img');
            var news_c_info = news_c.find('.right_info');
            console.log(news_c_img.outerHeight());
            console.log(news_c_info.outerHeight());
            if(news_c_info.outerHeight() > news_c_img.outerHeight()){

                news_c_img.css({
                    "vertical-align": "top"
                })
            } else {
                news_c_img.css({
                    "vertical-align": "middle"
                })
            }
        },
        map_init: function() {
            var map_option = {
                center: new google.maps.LatLng(23.8038036, 121.4565481),
                zoom: 15,
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                styles: [{
                    "featureType": "road",
                    "elementType": "geometry.fill",
                    "stylers": [{
                        "hue": "#ffdd00"
                    }, {
                        "saturation": -60
                    }, {
                        "lightness": 5
                    }, {
                        "gamma": 0.9
                    }, {
                        "visibility": "on"
                    }, {
                        "weight": 3.8
                    }]
                }, {
                    "featureType": "landscape",
                    "elementType": "geometry",
                    "stylers": [{
                        "color": "#e6e2d6"
                    }, {
                        "weight": 0.1
                    }]
                }, {
                    "featureType": "road",
                    "elementType": "geometry",
                    "stylers": [{
                        "color": "#aba286"
                    }, {
                        "visibility": "simplified"
                    }]
                }, {
                    "featureType": "water",
                    "elementType": "geometry",
                    "stylers": [{
                        "weight": 3.5
                    }, {
                        "color": "#b7985b"
                    }]
                }, {
                    "featureType": "road",
                    "elementType": "labels.text",
                    "stylers": [{
                        "visibility": "simplified"
                    }, {
                        "color": "#21251c"
                    }]
                }, {
                    "featureType": "poi",
                    "elementType": "geometry",
                    "stylers": [{
                        "hue": "#2a00ff"
                    }, {
                        "color": "#e3bf7b"
                    }]
                }, {
                    "featureType": "poi",
                    "elementType": "labels",
                    "stylers": [{
                        "visibility": "off"
                    }]
                }, {}]
            };
            var map = new google.maps.Map(document.body.getElementsByClassName("map")[0], map_option);
            var marker_image = {
                url: "assets/images/logo_2.png",
                size: new google.maps.Size(99, 80),
                origin: new google.maps.Point(0, 0),
                anchor: new google.maps.Point(50, 40)
            };
            var main_Position = new google.maps.LatLng(23.8038036, 121.4565481);
            var marker = new google.maps.Marker({
                position: main_Position,
                map: map,
                optimized: false,
                size: new google.maps.Size(99, 80),
                icon: marker_image
            });
        }
    }

    $(function() {
        main.menuToggle();
        main.index_slide();
        main.index_more();
        main.event_init();
        main.init();
        main.news_in();
        main.food_load();
        main.map_init();
        $(window).resize(function() {
            main.init();

        });
    });

})();
